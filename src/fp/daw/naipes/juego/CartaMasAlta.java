package fp.daw.naipes.juego;

import java.util.Scanner;

import fp.daw.naipes.Mazo;
import fp.daw.naipes.Naipe;
import fp.daw.naipes.Rango;

public class CartaMasAlta {
	

		public static void main(String[] args) {
//			Leer de teclado
			Scanner in = new Scanner(System.in);
//			Declaro objeto mazo
			Mazo mazo = new Mazo();
//			juego
			while (quieroJugar(in))
				jugar(mazo);
			System.out.println("Adios");
		}

//	métodos, trastear con el método quieroJugar 
		static boolean quieroJugar(Scanner in) {
			boolean quieroJugar;
			boolean correcta;
//			do {
//				System.out.print("¿Jugar? (s/n): ");
//				String respuesta = in.nextLine().toLowerCase();
//					if(respuesta== "s")
//					
//				}
//			}
			do {
				System.out.print("¿Jugar? (s/n): ");
				String respuesta = in.nextLine().toLowerCase();
				correcta = (quieroJugar = respuesta.equals("s")) || respuesta.equals("n");
				if (!correcta)
					System.out.println("Respuesta incorrecta");
			} while (!correcta);
			return quieroJugar;
		}
		
		static void jugar(Mazo mazo) {
			Naipe naipeJugador = mazo.get();
			int valorJugador = valor(naipeJugador);
			Naipe naipeOrdenador = mazo.get();
			int valorOrdenador = valor(naipeOrdenador);
			System.out.println("Jugador: " + naipeJugador);
			System.out.println("Ordenador:" + naipeOrdenador);
			if (valorJugador > valorOrdenador)
				System.out.println("Gana el jugador");
			else if (valorJugador< valorOrdenador)
				System.out.println("Gana el ordenador");
			else
				System.out.println("Empate");
		}
//		Sólo cambio esto
		static int valor(Naipe naipe) {
			return naipe.getRango() == Rango.A ? Integer.MAX_VALUE : naipe.getRango().ordinal();
		}
}
		